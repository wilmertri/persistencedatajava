package repositories;

import entities.Book;
import entities.Person;
import persistence.FileManage;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class DBBook {

    public ArrayList<Book> getDataBooks(){
        ArrayList<Book> books = null;
        FileManage fileManage = new FileManage("books.txt");
        ArrayList<String> lines = fileManage.getDataOfFile();
        if (lines != null){
            books = new ArrayList<>();
            for (String line : lines) {
                StringTokenizer tokens = new StringTokenizer(line, ";");
                int id = Integer.parseInt(tokens.nextToken());
                String name = tokens.nextToken();
                String synopsis = tokens.nextToken();
                books.add(new Book(id, name, synopsis));
            }
        }
        return books;
    }

    public Boolean insertBook(Book book){
        FileManage fileManage = new FileManage("books.txt");
        return fileManage.insertDataInFile(this.getLine(book));
    }

    public Boolean updateBook(Book oldBook, Book newBook){
        FileManage fileManage = new FileManage("books.txt");
        String lineOldBook = this.getLine(oldBook);
        String lineNewBook = this.getLine(newBook);
        return fileManage.updateDataInFile(lineOldBook, lineNewBook);
    }

    public void deleteBook(Book book){
        FileManage fileManage = new FileManage("books.txt");
        String lineBook = this.getLine(book);
        fileManage.deleteDataInFile(lineBook);
    }

    private String getLine(Book book){
        return book.getId() + ";" + book.getName() + ";" + book.getSynopsis();
    }
}
