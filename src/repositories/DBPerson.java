package repositories;

import entities.Person;
import persistence.FileManage;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class DBPerson {
    public ArrayList<Person> getDataPersons(){
        ArrayList<Person> persons = null;
        FileManage fileManage = new FileManage("people.txt");
        ArrayList<String> lines = fileManage.getDataOfFile();
        if (lines != null){
            persons = new ArrayList<>();
            for (String line : lines) {
                StringTokenizer tokens = new StringTokenizer(line, "|");
                int id = Integer.parseInt(tokens.nextToken());
                String name = tokens.nextToken();
                String lastName = tokens.nextToken();
                int age = Integer.parseInt(tokens.nextToken());
                persons.add(new Person(id, name, lastName, age));
            }
        }
        return persons;
    }

    public boolean addPerson(Person p){
        FileManage fileManage = new FileManage("people.txt");
        return fileManage.insertDataInFile(
                this.getNextId() + "|" + p.getName() + "|" + p.getLastName() + "|" + p.getAge()
        );
    }

    public boolean updatePerson(Person oldPerson, Person newPerson){
        FileManage fileManage = new FileManage("people.txt");
        String lineOldPerson = this.getLinePerson(oldPerson);
        String lineNewPerson = this.getLinePerson(newPerson);
        return fileManage.updateDataInFile(lineOldPerson, lineNewPerson);
    }

    private String getLinePerson(Person p){
        return p.getId() + "|" + p.getName() + "|" + p.getLastName() + "|" + p.getAge();
    }

    private int getNextId(){
        return this.getDataPersons().size() + 1;
    }
}
