package controller;

import entities.Person;
import repositories.DBPerson;

import javax.swing.*;
import java.util.ArrayList;

public class PersonManageController {
    private ArrayList<Person> persons;

    public void addPerson(Person person){
        this.persons.add(person);
    }

    public void deletePerson(int index){
        this.persons.remove(index);
    }

    public int totalPersons(){
        return this.persons.size();
    }

    public Person getPerson(int index){
        return this.persons.get(index);
    }

    public ArrayList<Person> getPeople(){
        return this.persons;
    }

    public void chargePersons(){
        DBPerson db = new DBPerson();
        this.persons = db.getDataPersons();
    }

    public void printDataPersons(){
        for (Person p: this.persons){
            System.out.println("------------------------");
            System.out.println(p.getId() + " - " + p.getName() + " " + p.getLastName());
        }
    }

    public boolean insertPerson(Person p){
        DBPerson db = new DBPerson();
        return db.addPerson(p);
    }

    public boolean updatePerson(Person oldPerson, Person newPerson){
        DBPerson db = new DBPerson();
        return db.updatePerson(oldPerson, newPerson);
    }
}
