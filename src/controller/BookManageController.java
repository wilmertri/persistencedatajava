package controller;

import entities.Book;
import entities.Person;
import repositories.DBBook;
import repositories.DBPerson;

import java.util.ArrayList;

public class BookManageController {
    private ArrayList<Book> books;
    private DBBook dbBook;

    public BookManageController(){
        this.dbBook = new DBBook();
    }

    public ArrayList<Book> getBooks(){
        return this.books;
    }

    public void chargeBooks(){
        this.books = this.dbBook.getDataBooks();
    }

    public boolean insertBook(Book book){
        return this.dbBook.insertBook(book);
    }
    public boolean updateBook(Book oldBook, Book newBook){
        return this.dbBook.updateBook(oldBook, newBook);
    }

    public void deleteBook(Book book)
    {
        this.dbBook.deleteBook(book);
    }

    public void printDataBooks(){
        for (Book b: this.books){
            System.out.println("------------------------");
            System.out.println(b.getId() + " - " + b.getName() + " - " + b.getSynopsis());
        }
    }

}
