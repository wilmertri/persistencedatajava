import controller.BookManageController;
import controller.PersonManageController;
import entities.Book;
import entities.Person;
import repositories.DBPerson;

import javax.swing.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        PersonManageController p = new PersonManageController();
        //p.chargePersons();
        //p.printDataPersons();
        /**
        Person person = new Person("Mario", "Perez", 18);
        if (p.insertPerson(person)){
            JOptionPane.showMessageDialog(null, "The person was create successfully!");
        }
        p.chargePersons();
        p.printDataPersons();

        ArrayList<Person> people = p.getPeople();
        Person oldPerson = people.get(1);
        Person newPerson = new Person(oldPerson.getId(),"Camilo", "Vargas", 28);
        if(p.updatePerson(oldPerson, newPerson)){
            JOptionPane.showMessageDialog(null, "The person was update successfully!");
        };
         **/
        BookManageController b = new BookManageController();
        //Book newBook = new Book(4, "Cien Años de Soledad", "Gabriel Garcia Marquez");
        //books.insertBook(newBook);
        b.chargeBooks();
        b.printDataBooks();

        ArrayList<Book> books = b.getBooks();
        Book oldBook = books.get(2);
        Book newBook = new Book(oldBook.getId(), "El Libro de la vida", "Sinopsis 2");
        if(b.updateBook(oldBook, newBook)){
            JOptionPane.showMessageDialog(null, "The book was update successfully!");
        };
        b.chargeBooks();
        b.printDataBooks();
        Book book = books.get(1);
        b.deleteBook(book);
        b.chargeBooks();
        b.printDataBooks();







    }
}